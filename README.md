# jugs
A declarative build system and package manager for Urn.  
  
# Prerequisites
- Unix-like operating system
- Lua (5.1/5.2/5.3/LuaJIT)
- Git
- Wget or Curl
  
# Installation
You can download jugs from this repository:  
`wget https://gitlab.com/urn/jugs/raw/master/bin/jugs.lua`
You can then make it executable:  
`chmod +x jugs.lua`  
Or you can run it with your lua interpreter.  
It is advisable to then make a `jugs` alias or add it to a PATH directory,
so you can run it anywhere using `jugs <...>`.  
  
# Usage
You can get a basic template by running `jugs init <name>`, which makes a basic Jugsfile and a source file.
## Jugsfile
In order to build a program, you need to make a description file called `jugs.lisp`.  
An example jugs.lisp:  
```
(pkg
  :name "example"                ; The name of the program
  :authors ("Joe")               ; The authors
  :deps ("urntils")              ; The dependencies of this program
  :entry ( :main "main.lisp" ) ) ; The entry file (default: main), which get passed to the urn compiler
```
## Building
When you're done making the jugs.lisp, you can build your program:  
`jugs build`  
Jugs then downloads the package index, Urn compiler, sets up the environment and builds the program.  
There you go, the executable is located at `.jugs/example.lua`.  
You can build and run the program at once with:  
`jugs run`  
## Updating
Jugs doesn't update the package index automatically.  
If you want to update the package index, either from the default location or from a custom path, do:  
`jugs update-index [path/url]`  
## Interactive testing  
You can launch the urn REPL and import your source files so you can interactively test components of your program:
`jugs repl`
