#!/usr/bin/env lua
if not table.pack then table.pack = function(...) return { n = select("#", ...), ... } end end
if not table.unpack then table.unpack = unpack end
local load = load if _VERSION:find("5.1") then load = function(x, n, _, env) local f, e = loadstring(x, n) if not f then error(e, 2) end if env then setfenv(f, env) end return f end end
local _select, _unpack, _pack, _error = select, table.unpack, table.pack, error
local _libs = {}
local _temp = (function()
	return {
		['slice'] = function(xs, start, finish)
			if not finish then finish = xs.n end
			if not finish then finish = #xs end
			return { tag = "list", n = finish - start + 1, table.unpack(xs, start, finish) }
		end,
	}
end)()
for k, v in pairs(_temp) do _libs["lua/basic-0/".. k] = v end
local _3d_1, _2f3d_1, _3c_1, _3c3d_1, _3e_1, _3e3d_1, _2b_1, _2d_1, _25_1, _2e2e_1, arg_23_1, len_23_1, slice1, error1, getmetatable1, print1, getIdx1, setIdx_21_1, tonumber1, tostring1, type_23_1, n1, char1, find1, format1, gsub1, lower1, match1, sub1, upper1, concat1, unpack1, iterPairs1, car1, cdr1, list1, cons1, _21_1, pretty1, arg1, apply1, list_3f_1, empty_3f_1, string_3f_1, number_3f_1, nil_3f_1, between_3f_1, type1, eq_3f_1, neq_3f_1, min1, car2, cdr2, cons2, map1, filter1, last1, nth1, nths1, pushCdr_21_1, popLast_21_1, takeWhile1, split1, cadr1, caddr1, charAt1, _2e2e_2, split2, quoted1, execute1, exit1, getenv1, keys1, open1, popen1, succ1, pred1, fail_21_1, exit_21_1, self1, config1, coloredAnsi1, colored_3f_1, colored1, printError_21_1, printExit_21_1, readAllMode_21_1, readAll_21_1, writeAllMode_21_1, writeAll_21_1, create1, setAction1, addAction1, addArgument_21_1, addHelp_21_1, helpNarg_21_1, usage_21_1, usageError_21_1, help_21_1, matcher1, parse_21_1, args1, pkgDir1, execCmdCode_21_1, execCmd_21_1, absPath1, readUrl_21_1, readPath_21_1, writeFile_21_1, pathExists_3f_1, dirExists_3f_1, makeDir_21_1, copyDir_21_1, rmDir_21_1, rmFile_21_1, cloneGit_21_1, cache1, env1, indexPrev1, compiler1, putNodeWarning_21_1, doNodeError_21_1, void1, getSource1, hexDigit_3f_1, binDigit_3f_1, terminator_3f_1, digitError_21_1, eofError_21_1, lex1, parse1, read1, index1, desc1, unquoteList1, quotedlist_2d3e_struct1, parseEntry1, setupEnv_21_1, loadIndex_21_1, loadDesc_21_1, installPkgs_21_1, argsFlags1, urnFlags1, runtimeArgs1, placePkg_21_1, initPkg_21_1, buildPkg_21_1, runPkg_21_1, runRepl_21_1, run_21_1
_3d_1 = function(v1, v2) return (v1 == v2) end
_2f3d_1 = function(v1, v2) return (v1 ~= v2) end
_3c_1 = function(v1, v2) return (v1 < v2) end
_3c3d_1 = function(v1, v2) return (v1 <= v2) end
_3e_1 = function(v1, v2) return (v1 > v2) end
_3e3d_1 = function(v1, v2) return (v1 >= v2) end
_2b_1 = function(v1, v2, ...) local t = (v1 + v2) for i = 1, _select('#', ...) do t = (t + _select(i, ...)) end return t end
_2d_1 = function(v1, v2, ...) local t = (v1 - v2) for i = 1, _select('#', ...) do t = (t - _select(i, ...)) end return t end
_25_1 = function(v1, v2, ...) local t = (v1 % v2) for i = 1, _select('#', ...) do t = (t % _select(i, ...)) end return t end
_2e2e_1 = function(v1, v2, ...) local t = (v1 .. v2) for i = _select('#', ...), 1, -1 do t = (_select(i, ...) .. t) end return t end
arg_23_1 = arg
len_23_1 = function(v1) return #(v1) end
slice1 = _libs["lua/basic-0/slice"]
error1 = error
getmetatable1 = getmetatable
print1 = print
getIdx1 = function(v1, v2) return v1[v2] end
setIdx_21_1 = function(v1, v2, v3) v1[v2] = v3 end
tonumber1 = tonumber
tostring1 = tostring
type_23_1 = type
n1 = (function(x)
	if (type_23_1(x) == "table") then
		return x["n"]
	else
		return #(x)
	end
end)
char1 = string.char
find1 = string.find
format1 = string.format
gsub1 = string.gsub
lower1 = string.lower
match1 = string.match
sub1 = string.sub
upper1 = string.upper
concat1 = table.concat
unpack1 = table.unpack
iterPairs1 = function(x, f) for k, v in pairs(x) do f(k, v) end end
car1 = (function(xs)
	return xs[1]
end)
cdr1 = (function(xs)
	return slice1(xs, 2)
end)
list1 = (function(...)
	local xs = _pack(...) xs.tag = "list"
	return xs
end)
cons1 = (function(x, xs)
	local _offset, _result, _temp = 0, {tag="list",n=0}
	_result[1 + _offset] = x
	_temp = xs
	for _c = 1, _temp.n do _result[1 + _c + _offset] = _temp[_c] end
	_offset = _offset + _temp.n
	_result.n = _offset + 1
	return _result
end)
_21_1 = (function(expr)
	return not expr
end)
pretty1 = (function(value)
	local ty = type_23_1(value)
	if (ty == "table") then
		local tag = value["tag"]
		if (tag == "list") then
			local out = ({tag = "list", n = 0})
			local temp = n1(value)
			local temp1 = nil
			local temp2 = 1
			while (temp2 <= temp) do
				out[temp2] = pretty1(value[temp2])
				temp2 = (temp2 + 1)
			end
			return ("(" .. (concat1(out, " ") .. ")"))
		elseif ((type_23_1(getmetatable1(value)) == "table") and (type_23_1(getmetatable1(value)["--pretty-print"]) == "function")) then
			return getmetatable1(value)["--pretty-print"](value)
		elseif (tag == "list") then
			return value["contents"]
		elseif (tag == "symbol") then
			return value["contents"]
		elseif (tag == "key") then
			return (":" .. value["value"])
		elseif (tag == "string") then
			return format1("%q", value["value"])
		elseif (tag == "number") then
			return tostring1(value["value"])
		else
			local out = ({tag = "list", n = 0})
			iterPairs1(value, (function(k, v)
				out = cons1((pretty1(k) .. (" " .. pretty1(v))), out)
				return nil
			end))
			return ("{" .. (concat1(out, " ") .. "}"))
		end
	elseif (ty == "string") then
		return format1("%q", value)
	else
		return tostring1(value)
	end
end)
if (nil == arg_23_1) then
	arg1 = ({tag = "list", n = 0})
else
	arg_23_1["tag"] = "list"
	if arg_23_1["n"] then
	else
		arg_23_1["n"] = #(arg_23_1)
	end
	arg1 = arg_23_1
end
apply1 = (function(f, xs)
	return f(unpack1(xs, 1, n1(xs)))
end)
list_3f_1 = (function(x)
	return (type1(x) == "list")
end)
empty_3f_1 = (function(x)
	local xt = type1(x)
	if (xt == "list") then
		return (n1(x) == 0)
	elseif (xt == "string") then
		return (#(x) == 0)
	else
		return false
	end
end)
string_3f_1 = (function(x)
	return ((type_23_1(x) == "string") or ((type_23_1(x) == "table") and (x["tag"] == "string")))
end)
number_3f_1 = (function(x)
	return ((type_23_1(x) == "number") or ((type_23_1(x) == "table") and (x["tag"] == "number")))
end)
nil_3f_1 = (function(x)
	return (type_23_1(x) == "nil")
end)
between_3f_1 = (function(val, min, max)
	return ((val >= min) and (val <= max))
end)
type1 = (function(val)
	local ty = type_23_1(val)
	if (ty == "table") then
		return (val["tag"] or "table")
	else
		return ty
	end
end)
eq_3f_1 = (function(x, y)
	if (x == y) then
		return true
	else
		local typeX = type1(x)
		local typeY = type1(y)
		if ((typeX == "list") and ((typeY == "list") and (n1(x) == n1(y)))) then
			local equal = true
			local temp = n1(x)
			local temp1 = nil
			local temp2 = 1
			while (temp2 <= temp) do
				if neq_3f_1(x[temp2], y[temp2]) then
					equal = false
				end
				temp2 = (temp2 + 1)
			end
			return equal
		elseif (("table" == type_23_1(x)) and getmetatable1(x)) then
			return getmetatable1(x)["compare"](x, y)
		elseif (("table" == typeX) and ("table" == typeY)) then
			local equal = true
			iterPairs1(x, (function(k, v)
				if neq_3f_1(v, y[k]) then
					equal = false
					return nil
				else
					return nil
				end
			end))
			return equal
		elseif (("symbol" == typeX) and ("symbol" == typeY)) then
			return (x["contents"] == y["contents"])
		elseif (("key" == typeX) and ("key" == typeY)) then
			return (x["value"] == y["value"])
		elseif (("symbol" == typeX) and ("string" == typeY)) then
			return (x["contents"] == y)
		elseif (("string" == typeX) and ("symbol" == typeY)) then
			return (x == y["contents"])
		elseif (("key" == typeX) and ("string" == typeY)) then
			return (x["value"] == y)
		elseif (("string" == typeX) and ("key" == typeY)) then
			return (x == y["value"])
		else
			return false
		end
	end
end)
neq_3f_1 = (function(x, y)
	return _21_1(eq_3f_1(x, y))
end)
min1 = math.min
car2 = (function(x)
	local temp = type1(x)
	if (temp ~= "list") then
		error1(format1("bad argument %s (expected %s, got %s)", "x", "list", temp), 2)
	end
	return car1(x)
end)
cdr2 = (function(x)
	local temp = type1(x)
	if (temp ~= "list") then
		error1(format1("bad argument %s (expected %s, got %s)", "x", "list", temp), 2)
	end
	if empty_3f_1(x) then
		return ({tag = "list", n = 0})
	else
		return cdr1(x)
	end
end)
cons2 = (function(...)
	local _n = _select("#", ...) - 1
	local xs, xss
	if _n > 0 then
		xs = { tag="list", n=_n, _unpack(_pack(...), 1, _n)}
		xss = select(_n + 1, ...)
	else
		xs = { tag="list", n=0}
		xss = ...
	end
	local _offset, _result, _temp = 0, {tag="list",n=0}
	_temp = xs
	for _c = 1, _temp.n do _result[0 + _c + _offset] = _temp[_c] end
	_offset = _offset + _temp.n
	_temp = xss
	for _c = 1, _temp.n do _result[0 + _c + _offset] = _temp[_c] end
	_offset = _offset + _temp.n
	_result.n = _offset + 0
	return _result
end)
map1 = (function(fn, ...)
	local xss = _pack(...) xss.tag = "list"
	local ns
	local out = ({tag = "list", n = 0})
	local temp = n1(xss)
	local temp1 = nil
	local temp2 = 1
	while (temp2 <= temp) do
		if _21_1(list_3f_1(nth1(xss, temp2))) then
			error1(("not a list: " .. (pretty1(nth1(xss, temp2)) .. (" (it's a " .. (type1(nth1(xss, temp2)) .. ")")))))
		else
		end
		pushCdr_21_1(out, n1(nth1(xss, temp2)))
		temp2 = (temp2 + 1)
	end
	ns = out
	local out = ({tag = "list", n = 0})
	local temp = apply1(min1, ns)
	local temp1 = nil
	local temp2 = 1
	while (temp2 <= temp) do
		pushCdr_21_1(out, apply1(fn, nths1(xss, temp2)))
		temp2 = (temp2 + 1)
	end
	return out
end)
filter1 = (function(p, xs)
	local temp = type1(p)
	if (temp ~= "function") then
		error1(format1("bad argument %s (expected %s, got %s)", "p", "function", temp), 2)
	end
	local temp = type1(xs)
	if (temp ~= "list") then
		error1(format1("bad argument %s (expected %s, got %s)", "xs", "list", temp), 2)
	end
	local out = ({tag = "list", n = 0})
	local temp = n1(xs)
	local temp1 = nil
	local temp2 = 1
	while (temp2 <= temp) do
		local x = nth1(xs, temp2)
		if p(x) then
			pushCdr_21_1(out, x)
		end
		temp2 = (temp2 + 1)
	end
	return out
end)
last1 = (function(xs)
	local temp = type1(xs)
	if (temp ~= "list") then
		error1(format1("bad argument %s (expected %s, got %s)", "xs", "list", temp), 2)
	end
	return xs[n1(xs)]
end)
nth1 = (function(xs, idx)
	return xs[idx]
end)
nths1 = (function(xss, idx)
	local out = ({tag = "list", n = 0})
	local temp = n1(xss)
	local temp1 = nil
	local temp2 = 1
	while (temp2 <= temp) do
		pushCdr_21_1(out, nth1(nth1(xss, temp2), idx))
		temp2 = (temp2 + 1)
	end
	return out
end)
pushCdr_21_1 = (function(xs, val)
	local temp = type1(xs)
	if (temp ~= "list") then
		error1(format1("bad argument %s (expected %s, got %s)", "xs", "list", temp), 2)
	end
	local len = (n1(xs) + 1)
	xs["n"] = len
	xs[len] = val
	return xs
end)
popLast_21_1 = (function(xs)
	local temp = type1(xs)
	if (temp ~= "list") then
		error1(format1("bad argument %s (expected %s, got %s)", "xs", "list", temp), 2)
	end
	local x = xs[n1(xs)]
	xs[n1(xs)] = nil
	xs["n"] = (n1(xs) - 1)
	return x
end)
takeWhile1 = (function(p, xs, idx)
	local temp = type1(p)
	if (temp ~= "function") then
		error1(format1("bad argument %s (expected %s, got %s)", "p", "function", temp), 2)
	end
	local temp = type1(xs)
	if (temp ~= "list") then
		error1(format1("bad argument %s (expected %s, got %s)", "xs", "list", temp), 2)
	end
	if (type1(idx) == "number") then
	else
		idx = 1
	end
	local l = ({tag = "list", n = 0})
	local ln = n1(xs)
	local x = nth1(xs, idx)
	if nil_3f_1(x) then
		return ({tag = "list", n = 0})
	else
		local temp = nil
		while ((idx <= ln) and p(x)) do
			pushCdr_21_1(l, x)
			idx = (idx + 1)
			x = nth1(xs, idx)
		end
		return l
	end
end)
split1 = (function(xs, y)
	local temp = type1(xs)
	if (temp ~= "list") then
		error1(format1("bad argument %s (expected %s, got %s)", "xs", "list", temp), 2)
	end
	local l = ({tag = "list", n = 0})
	local p = (function(x)
		return neq_3f_1(x, y)
	end)
	local idx = 1
	local b = takeWhile1(p, xs, idx)
	local temp = nil
	while _21_1(empty_3f_1(b)) do
		pushCdr_21_1(l, b)
		idx = ((idx + n1(b)) + 1)
		b = takeWhile1(p, xs, idx)
	end
	return l
end)
cadr1 = (function(x)
	return car2(cdr2(x))
end)
caddr1 = (function(x)
	return car2(cdr2(cdr2(x)))
end)
charAt1 = (function(xs, x)
	return sub1(xs, x, x)
end)
_2e2e_2 = (function(...)
	local args = _pack(...) args.tag = "list"
	return concat1(args)
end)
split2 = (function(text, pattern, limit)
	local out = ({tag = "list", n = 0})
	local loop = true
	local start = 1
	local temp = nil
	while loop do
		local pos = list1(find1(text, pattern, start))
		local nstart = car2(pos)
		local nend = cadr1(pos)
		if ((nstart == nil) or (limit and (n1(out) >= limit))) then
			loop = false
			pushCdr_21_1(out, sub1(text, start, n1(text)))
			start = (n1(text) + 1)
		elseif (nstart > #(text)) then
			if (start <= #(text)) then
				pushCdr_21_1(out, sub1(text, start, #(text)))
			end
			loop = false
		elseif (nend < nstart) then
			pushCdr_21_1(out, sub1(text, start, nstart))
			start = (nstart + 1)
		else
			pushCdr_21_1(out, sub1(text, start, (nstart - 1)))
			start = (nend + 1)
		end
	end
	return out
end)
local escapes = ({})
local temp = nil
local temp1 = 0
while (temp1 <= 31) do
	escapes[char1(temp1)] = _2e2e_2("\\", tostring1(temp1))
	temp1 = (temp1 + 1)
end
escapes["\n"] = "n"
quoted1 = (function(str)
	return (gsub1(format1("%q", str), ".", escapes))
end)
execute1 = os.execute
exit1 = os.exit
getenv1 = os.getenv
keys1 = (function(st)
	local out = ({tag = "list", n = 0})
	iterPairs1(st, (function(k, _5f_)
		return pushCdr_21_1(out, k)
	end))
	return out
end)
open1 = io.open
popen1 = io.popen
succ1 = (function(x)
	return (x + 1)
end)
pred1 = (function(x)
	return (x - 1)
end)
fail_21_1 = (function(x)
	return error1(x, 0)
end)
exit_21_1 = (function(reason, code)
	local code1
	if string_3f_1(reason) then
		code1 = code
	else
		code1 = reason
	end
	if string_3f_1(reason) then
		print1(reason)
	end
	return exit1(code1)
end)
self1 = (function(x, key, ...)
	local args = _pack(...) args.tag = "list"
	return x[key](x, unpack1(args, 1, n1(args)))
end)
config1 = package.config
coloredAnsi1 = (function(col, msg)
	return _2e2e_2("\27[", col, "m", msg, "\27[0m")
end)
if (config1 and (charAt1(config1, 1) ~= "\\")) then
	colored_3f_1 = true
elseif (getenv1 and (getenv1("ANSICON") ~= nil)) then
	colored_3f_1 = true
else
	local temp
	if getenv1 then
		local term = getenv1("TERM")
		if term then
			temp = find1(term, "xterm")
		else
			temp = nil
		end
	else
		temp = false
	end
	if temp then
		colored_3f_1 = true
	else
		colored_3f_1 = false
	end
end
if colored_3f_1 then
	colored1 = coloredAnsi1
else
	colored1 = (function(col, msg)
		return msg
	end)
end
printError_21_1 = (function(msg)
	print1(_2e2e_2(colored1(31, "[ERROR] "), msg))
	return exit1(1)
end)
printExit_21_1 = (function(msg)
	print1(msg)
	return exit1(1)
end)
readAllMode_21_1 = (function(path, binary)
	local handle = open1(path, _2e2e_2("r", (function()
		if binary then
			return "b"
		else
			return ""
		end
	end)()
	))
	if handle then
		local data = self1(handle, "read", "*all")
		if data then
			self1(handle, "close")
			return data
		else
			return nil
		end
	else
		return nil
	end
end)
readAll_21_1 = (function(path)
	return readAllMode_21_1(path, false)
end)
writeAllMode_21_1 = (function(path, append, binary, data)
	local handle = open1(path, _2e2e_2((function()
		if append then
			return "a"
		else
			return "w"
		end
	end)()
	, (function()
		if binary then
			return "b"
		else
			return ""
		end
	end)()
	))
	if handle then
		self1(handle, "write", data)
		self1(handle, "close")
		return true
	else
		return false
	end
end)
writeAll_21_1 = (function(path, data)
	return writeAllMode_21_1(path, false, false, data)
end)
create1 = (function(description)
	return ({["desc"]=description,["flag-map"]=({}),["opt-map"]=({}),["opt"]=({tag = "list", n = 0}),["pos"]=({tag = "list", n = 0})})
end)
setAction1 = (function(arg, data, value)
	data[arg["name"]] = value
	return nil
end)
addAction1 = (function(arg, data, value)
	local lst = data[arg["name"]]
	if lst then
	else
		lst = ({tag = "list", n = 0})
		data[arg["name"]] = lst
	end
	return pushCdr_21_1(lst, value)
end)
addArgument_21_1 = (function(spec, names, ...)
	local options = _pack(...) options.tag = "list"
	local temp = type1(names)
	if (temp ~= "list") then
		error1(format1("bad argument %s (expected %s, got %s)", "names", "list", temp), 2)
	end
	if empty_3f_1(names) then
		error1("Names list is empty")
	end
	if ((n1(options) % 2) == 0) then
	else
		error1("Options list should be a multiple of two")
	end
	local result = ({["names"]=names,["action"]=nil,["narg"]=0,["default"]=false,["help"]="",["value"]=true})
	local first = car2(names)
	if (sub1(first, 1, 2) == "--") then
		pushCdr_21_1(spec["opt"], result)
		result["name"] = sub1(first, 3)
	elseif (sub1(first, 1, 1) == "-") then
		pushCdr_21_1(spec["opt"], result)
		result["name"] = sub1(first, 2)
	else
		result["name"] = first
		result["narg"] = "*"
		result["default"] = ({tag = "list", n = 0})
		pushCdr_21_1(spec["pos"], result)
	end
	local temp = n1(names)
	local temp1 = nil
	local temp2 = 1
	while (temp2 <= temp) do
		local name = names[temp2]
		if (sub1(name, 1, 2) == "--") then
			spec["opt-map"][sub1(name, 3)] = result
		elseif (sub1(name, 1, 1) == "-") then
			spec["flag-map"][sub1(name, 2)] = result
		end
		temp2 = (temp2 + 1)
	end
	local temp = n1(options)
	local temp1 = nil
	local temp2 = 1
	while (temp2 <= temp) do
		local key = nth1(options, temp2)
		result[key] = (nth1(options, (temp2 + 1)))
		temp2 = (temp2 + 2)
	end
	if result["var"] then
	else
		result["var"] = upper1(result["name"])
	end
	if result["action"] then
	else
		result["action"] = (function()
			local temp
			if number_3f_1(result["narg"]) then
				temp = (result["narg"] <= 1)
			else
				temp = (result["narg"] == "?")
			end
			if temp then
				return setAction1
			else
				return addAction1
			end
		end)()
	end
	return result
end)
addHelp_21_1 = (function(spec)
	return addArgument_21_1(spec, ({tag = "list", n = 2, "--help", "-h"}), "help", "Show this help message", "default", nil, "value", nil, "action", (function(arg, result, value)
		help_21_1(spec)
		return exit_21_1(0)
	end))
end)
helpNarg_21_1 = (function(buffer, arg)
	local temp = arg["narg"]
	if (temp == "?") then
		return pushCdr_21_1(buffer, _2e2e_2(" [", arg["var"], "]"))
	elseif (temp == "*") then
		return pushCdr_21_1(buffer, _2e2e_2(" [", arg["var"], "...]"))
	elseif (temp == "+") then
		return pushCdr_21_1(buffer, _2e2e_2(" ", arg["var"], " [", arg["var"], "...]"))
	else
		local temp1 = nil
		local temp2 = 1
		while (temp2 <= temp) do
			pushCdr_21_1(buffer, _2e2e_2(" ", arg["var"]))
			temp2 = (temp2 + 1)
		end
		return nil
	end
end)
usage_21_1 = (function(spec, name)
	if name then
	else
		name = (nth1(arg1, 0) or (nth1(arg1, -1) or "?"))
	end
	local usage = list1("usage: ", name)
	local temp = spec["opt"]
	local temp1 = n1(temp)
	local temp2 = nil
	local temp3 = 1
	while (temp3 <= temp1) do
		local arg = temp[temp3]
		pushCdr_21_1(usage, _2e2e_2(" [", car2(arg["names"])))
		helpNarg_21_1(usage, arg)
		pushCdr_21_1(usage, "]")
		temp3 = (temp3 + 1)
	end
	local temp = spec["pos"]
	local temp1 = n1(temp)
	local temp2 = nil
	local temp3 = 1
	while (temp3 <= temp1) do
		helpNarg_21_1(usage, (temp[temp3]))
		temp3 = (temp3 + 1)
	end
	return print1(concat1(usage))
end)
usageError_21_1 = (function(spec, name, error)
	usage_21_1(spec, name)
	print1(error)
	return exit_21_1(1)
end)
help_21_1 = (function(spec, name)
	if name then
	else
		name = (nth1(arg1, 0) or (nth1(arg1, -1) or "?"))
	end
	usage_21_1(spec, name)
	if spec["desc"] then
		print1()
		print1(spec["desc"])
	end
	local max = 0
	local temp = spec["pos"]
	local temp1 = n1(temp)
	local temp2 = nil
	local temp3 = 1
	while (temp3 <= temp1) do
		local arg = temp[temp3]
		local len = n1(arg["var"])
		if (len > max) then
			max = len
		end
		temp3 = (temp3 + 1)
	end
	local temp = spec["opt"]
	local temp1 = n1(temp)
	local temp2 = nil
	local temp3 = 1
	while (temp3 <= temp1) do
		local arg = temp[temp3]
		local len = n1(concat1(arg["names"], ", "))
		if (len > max) then
			max = len
		end
		temp3 = (temp3 + 1)
	end
	local fmt = _2e2e_2(" %-", tostring1((max + 1)), "s %s")
	if empty_3f_1(spec["pos"]) then
	else
		print1()
		print1("Positional arguments")
		local temp = spec["pos"]
		local temp1 = n1(temp)
		local temp2 = nil
		local temp3 = 1
		while (temp3 <= temp1) do
			local arg = temp[temp3]
			print1(format1(fmt, arg["var"], arg["help"]))
			temp3 = (temp3 + 1)
		end
	end
	if empty_3f_1(spec["opt"]) then
		return nil
	else
		print1()
		print1("Optional arguments")
		local temp = spec["opt"]
		local temp1 = n1(temp)
		local temp2 = nil
		local temp3 = 1
		while (temp3 <= temp1) do
			local arg = temp[temp3]
			print1(format1(fmt, concat1(arg["names"], ", "), arg["help"]))
			temp3 = (temp3 + 1)
		end
		return nil
	end
end)
matcher1 = (function(pattern)
	return (function(x)
		local res = list1(match1(x, pattern))
		if (car2(res) == nil) then
			return nil
		else
			return res
		end
	end)
end)
parse_21_1 = (function(spec, args)
	if args then
	else
		args = arg1
	end
	local result = ({})
	local pos = spec["pos"]
	local idx = 1
	local len = n1(args)
	local usage_21_ = (function(msg)
		return usageError_21_1(spec, nth1(args, 0), msg)
	end)
	local action = (function(arg, value)
		return arg["action"](arg, result, value, usage_21_)
	end)
	local readArgs = (function(key, arg)
		local temp = arg["narg"]
		if (temp == "+") then
			idx = (idx + 1)
			local elem = nth1(args, idx)
			if (elem == nil) then
				usage_21_(_2e2e_2("Expected ", arg["var"], " after --", key, ", got nothing"))
			elseif (_21_1(arg["all"]) and find1(elem, "^%-")) then
				usage_21_(_2e2e_2("Expected ", arg["var"], " after --", key, ", got ", nth1(args, idx)))
			else
				action(arg, elem)
			end
			local running = true
			local temp1 = nil
			while running do
				idx = (idx + 1)
				local elem = nth1(args, idx)
				if (elem == nil) then
					running = false
				elseif (_21_1(arg["all"]) and find1(elem, "^%-")) then
					running = false
				else
					action(arg, elem)
				end
			end
			return nil
		elseif (temp == "*") then
			local running = true
			local temp1 = nil
			while running do
				idx = (idx + 1)
				local elem = nth1(args, idx)
				if (elem == nil) then
					running = false
				elseif (_21_1(arg["all"]) and find1(elem, "^%-")) then
					running = false
				else
					action(arg, elem)
				end
			end
			return nil
		elseif (temp == "?") then
			idx = (idx + 1)
			local elem = nth1(args, idx)
			if ((elem == nil) or (_21_1(arg["all"]) and find1(elem, "^%-"))) then
				return arg["action"](arg, result, arg["value"])
			else
				idx = (idx + 1)
				return action(arg, elem)
			end
		elseif (temp == 0) then
			idx = (idx + 1)
			return action(arg, arg["value"])
		else
			local temp1 = nil
			local temp2 = 1
			while (temp2 <= temp) do
				idx = (idx + 1)
				local elem = nth1(args, idx)
				if (elem == nil) then
					usage_21_(_2e2e_2("Expected ", temp, " args for ", key, ", got ", pred1(temp2)))
				elseif (_21_1(arg["all"]) and find1(elem, "^%-")) then
					usage_21_(_2e2e_2("Expected ", temp, " for ", key, ", got ", pred1(temp2)))
				else
					action(arg, elem)
				end
				temp2 = (temp2 + 1)
			end
			idx = (idx + 1)
			return nil
		end
	end)
	local temp = nil
	while (idx <= len) do
		local temp1 = nth1(args, idx)
		local temp2
		local temp3 = matcher1("^%-%-([^=]+)=(.+)$")(temp1)
		temp2 = (list_3f_1(temp3) and ((n1(temp3) >= 2) and ((n1(temp3) <= 2) and true)))
		if temp2 then
			local key = nth1(matcher1("^%-%-([^=]+)=(.+)$")(temp1), 1)
			local val = nth1(matcher1("^%-%-([^=]+)=(.+)$")(temp1), 2)
			local arg = spec["opt-map"][key]
			if (arg == nil) then
				usage_21_(_2e2e_2("Unknown argument ", key, " in ", nth1(args, idx)))
			elseif (_21_1(arg["many"]) and (nil ~= result[arg["name"]])) then
				usage_21_(_2e2e_2("Too may values for ", key, " in ", nth1(args, idx)))
			else
				local narg = arg["narg"]
				if (number_3f_1(narg) and (narg ~= 1)) then
					usage_21_(_2e2e_2("Expected ", tostring1(narg), " values, got 1 in ", nth1(args, idx)))
				end
				action(arg, val)
			end
			idx = (idx + 1)
		else
			local temp2
			local temp3 = matcher1("^%-%-(.*)$")(temp1)
			temp2 = (list_3f_1(temp3) and ((n1(temp3) >= 1) and ((n1(temp3) <= 1) and true)))
			if temp2 then
				local key = nth1(matcher1("^%-%-(.*)$")(temp1), 1)
				local arg = spec["opt-map"][key]
				if (arg == nil) then
					usage_21_(_2e2e_2("Unknown argument ", key, " in ", nth1(args, idx)))
				elseif (_21_1(arg["many"]) and (nil ~= result[arg["name"]])) then
					usage_21_(_2e2e_2("Too may values for ", key, " in ", nth1(args, idx)))
				else
					readArgs(key, arg)
				end
			else
				local temp2
				local temp3 = matcher1("^%-(.+)$")(temp1)
				temp2 = (list_3f_1(temp3) and ((n1(temp3) >= 1) and ((n1(temp3) <= 1) and true)))
				if temp2 then
					local flags = nth1(matcher1("^%-(.+)$")(temp1), 1)
					local i = 1
					local s = n1(flags)
					local temp2 = nil
					while (i <= s) do
						local key = charAt1(flags, i)
						local arg = spec["flag-map"][key]
						if (arg == nil) then
							usage_21_(_2e2e_2("Unknown flag ", key, " in ", nth1(args, idx)))
						elseif (_21_1(arg["many"]) and (nil ~= result[arg["name"]])) then
							usage_21_(_2e2e_2("Too many occurances of ", key, " in ", nth1(args, idx)))
						else
							local narg = arg["narg"]
							if (i == s) then
								readArgs(key, arg)
							elseif (narg == 0) then
								action(arg, arg["value"])
							else
								action(arg, sub1(flags, succ1(i)))
								i = succ1(s)
								idx = (idx + 1)
							end
						end
						i = (i + 1)
					end
				else
					local arg = car2(spec["pos"])
					if arg then
						action(arg, temp1)
					else
						usage_21_(_2e2e_2("Unknown argument ", arg))
					end
					idx = (idx + 1)
				end
			end
		end
	end
	local temp = spec["opt"]
	local temp1 = n1(temp)
	local temp2 = nil
	local temp3 = 1
	while (temp3 <= temp1) do
		local arg = temp[temp3]
		if (result[arg["name"]] == nil) then
			result[arg["name"]] = arg["default"]
		end
		temp3 = (temp3 + 1)
	end
	local temp = spec["pos"]
	local temp1 = n1(temp)
	local temp2 = nil
	local temp3 = 1
	while (temp3 <= temp1) do
		local arg = temp[temp3]
		if (result[arg["name"]] == nil) then
			result[arg["name"]] = arg["default"]
		end
		temp3 = (temp3 + 1)
	end
	return result
end)
local spec = create1()
addHelp_21_1(spec)
addArgument_21_1(spec, ({tag = "list", n = 1, "method"}), "help", _2e2e_2("The method:\n", "jugs init <name> \njugs build [entry]\njugs run [entry]\njugs repl\njugs update-index [path]"))
addArgument_21_1(spec, ({tag = "list", n = 2, "--dir", "-d"}), "help", "The program directory.", "default", ".", "narg", 1)
addArgument_21_1(spec, ({tag = "list", n = 1, "--flags"}), "help", "Flags to pass to the urn compiler.", "narg", "*", "all", true)
addArgument_21_1(spec, ({tag = "list", n = 2, "--runtime-args", "--"}), "help", "Arguments to pass to the program.", "narg", "*", "all", true)
args1 = parse_21_1(spec)
pkgDir1 = args1["dir"]
execCmdCode_21_1 = (function(command, verbose)
	return caddr1(list1(execute1(_2e2e_2(command, (function()
		if _21_1(verbose) then
			return " >/dev/null 2>&1"
		else
			return ""
		end
	end)()
	))))
end)
execCmd_21_1 = (function(command, verbose)
	return (execCmdCode_21_1(command, verbose) == 0)
end)
absPath1 = (function(path)
	if (charAt1(path, 1) == "/") then
		return path
	else
		return _2e2e_2(pkgDir1, "/", path)
	end
end)
readUrl_21_1 = (function(url)
	local program
	if execCmd_21_1("which curl") then
		program = "curl -s "
	else
		program = "wget -qO- "
	end
	local handle = popen1(_2e2e_2(program, url))
	if handle then
		local result = self1(handle, "read", "*a")
		self1(handle, "close")
		if (n1(result) ~= 0) then
			return result
		else
			return nil
		end
	else
		return nil
	end
end)
readPath_21_1 = (function(path)
	if ((sub1(path, 1, 7) == "http://") or (sub1(path, 1, 8) == "https://")) then
		return readUrl_21_1(path)
	else
		return readAll_21_1(absPath1(path))
	end
end)
writeFile_21_1 = (function(file, contents)
	return writeAll_21_1(absPath1(file), contents)
end)
pathExists_3f_1 = (function(path)
	return (readPath_21_1(path) ~= nil)
end)
dirExists_3f_1 = (function(dir)
	return execCmd_21_1(_2e2e_2("test -d ", absPath1(dir)))
end)
makeDir_21_1 = (function(dir)
	return execute1(_2e2e_2("mkdir ", absPath1(dir)))
end)
copyDir_21_1 = (function(from, to)
	return execCmd_21_1(_2e2e_2("cp -r ", absPath1(from), " ", absPath1(to)))
end)
rmDir_21_1 = (function(dir)
	return execCmd_21_1(_2e2e_2("rm -rf ", absPath1(dir)))
end)
rmFile_21_1 = (function(file)
	return execCmd_21_1(_2e2e_2("rm ", absPath1(file)))
end)
cloneGit_21_1 = (function(url, rev, path, destPath, cachePath)
	local localRepoPath = _2e2e_2(cachePath, "/", gsub1(url, "%W", "-"))
	local repoPath = absPath1(localRepoPath)
	if dirExists_3f_1(localRepoPath) then
		execCmd_21_1(_2e2e_2("cd ", repoPath, "; git checkout master"))
		execCmd_21_1(_2e2e_2("cd ", repoPath, "; git pull"))
	else
		execCmd_21_1(_2e2e_2("git clone ", url, " ", repoPath))
	end
	execCmd_21_1(_2e2e_2("cd ", repoPath, "; git checkout ", rev))
	return copyDir_21_1(_2e2e_2(localRepoPath, "/", path), destPath)
end)
cache1 = _2e2e_2(".jugs", "/cache")
env1 = _2e2e_2(".jugs", "/env")
indexPrev1 = _2e2e_2(".jugs", "/prev-index")
compiler1 = _2e2e_2(env1, "/urn/bin/urn.lua")
putNodeWarning_21_1 = (function(logger, msg, node, explain, ...)
	local lines = _pack(...) lines.tag = "list"
	return self1(logger, "put-node-warning!", msg, node, explain, lines)
end)
doNodeError_21_1 = (function(logger, msg, node, explain, ...)
	local lines = _pack(...) lines.tag = "list"
	self1(logger, "put-node-error!", msg, node, explain, lines)
	return fail_21_1((match1(msg, "^([^\n]+)\n") or msg))
end)
local discard = (function()
	return nil
end)
void1 = ({["put-error!"]=discard,["put-warning!"]=discard,["put-verbose!"]=discard,["put-debug!"]=discard,["put-time!"]=discard,["put-node-error!"]=discard,["put-node-warning!"]=discard})
getSource1 = (function(node)
	local result = nil
	local temp = nil
	while (node and _21_1(result)) do
		result = node["range"]
		node = node["parent"]
	end
	return result
end)
hexDigit_3f_1 = (function(char)
	return (between_3f_1(char, "0", "9") or (between_3f_1(char, "a", "f") or between_3f_1(char, "A", "F")))
end)
binDigit_3f_1 = (function(char)
	return ((char == "0") or (char == "1"))
end)
terminator_3f_1 = (function(char)
	return ((char == "\n") or ((char == " ") or ((char == "\9") or ((char == ";") or ((char == "(") or ((char == ")") or ((char == "[") or ((char == "]") or ((char == "{") or ((char == "}") or (char == "")))))))))))
end)
digitError_21_1 = (function(logger, pos, name, char)
	return doNodeError_21_1(logger, format1("Expected %s digit, got %s", name, (function()
		if (char == "") then
			return "eof"
		else
			return quoted1(char)
		end
	end)()
	), pos, nil, pos, "Invalid digit here")
end)
eofError_21_1 = (function(cont, logger, msg, node, explain, ...)
	local lines = _pack(...) lines.tag = "list"
	if cont then
		return fail_21_1(({["msg"]=msg,["cont"]=true}))
	else
		return doNodeError_21_1(logger, msg, node, explain, unpack1(lines, 1, n1(lines)))
	end
end)
lex1 = (function(logger, str, name, cont)
	str = gsub1(str, "\13\n?", "\n")
	local lines = split2(str, "\n")
	local line = 1
	local column = 1
	local offset = 1
	local length = n1(str)
	local out = ({tag = "list", n = 0})
	local consume_21_ = (function()
		if (charAt1(str, offset) == "\n") then
			line = (line + 1)
			column = 1
		else
			column = (column + 1)
		end
		offset = (offset + 1)
		return nil
	end)
	local position = (function()
		return ({["line"]=line,["column"]=column,["offset"]=offset})
	end)
	local range = (function(start, finish)
		return ({["start"]=start,["finish"]=(finish or start),["lines"]=lines,["name"]=name})
	end)
	local appendWith_21_ = (function(data, start, finish)
		local start1 = (start or position())
		local finish1 = (finish or position())
		data["range"] = range(start1, finish1)
		data["contents"] = sub1(str, start1["offset"], finish1["offset"])
		return pushCdr_21_1(out, data)
	end)
	local append_21_ = (function(tag, start, finish)
		return appendWith_21_(({["tag"]=tag}), start, finish)
	end)
	local parseBase = (function(name1, p, base)
		local start = offset
		local char = charAt1(str, offset)
		if p(char) then
		else
			digitError_21_1(logger, range(position()), name1, char)
		end
		char = charAt1(str, succ1(offset))
		local temp = nil
		while p(char) do
			consume_21_()
			char = charAt1(str, succ1(offset))
		end
		return tonumber1(sub1(str, start, offset), base)
	end)
	local temp = nil
	while (offset <= length) do
		local char = charAt1(str, offset)
		if ((char == "\n") or ((char == "\9") or (char == " "))) then
		elseif (char == "(") then
			appendWith_21_(({["tag"]="open",["close"]=")"}))
		elseif (char == ")") then
			appendWith_21_(({["tag"]="close",["open"]="("}))
		elseif (char == "[") then
			appendWith_21_(({["tag"]="open",["close"]="]"}))
		elseif (char == "]") then
			appendWith_21_(({["tag"]="close",["open"]="["}))
		elseif (char == "{") then
			appendWith_21_(({["tag"]="open-struct",["close"]="}"}))
		elseif (char == "}") then
			appendWith_21_(({["tag"]="close",["open"]="{"}))
		elseif (char == "'") then
			append_21_("quote")
		elseif (char == "`") then
			append_21_("syntax-quote")
		elseif (char == "~") then
			append_21_("quasiquote")
		elseif (char == ",") then
			if (charAt1(str, succ1(offset)) == "@") then
				local start = position()
				consume_21_()
				append_21_("unquote-splice", start)
			else
				append_21_("unquote")
			end
		elseif find1(str, "^%-?%.?[#0-9]", offset) then
			local start = position()
			local negative = (char == "-")
			if negative then
				consume_21_()
				char = charAt1(str, offset)
			end
			local val
			if ((char == "#") and (lower1(charAt1(str, succ1(offset))) == "x")) then
				consume_21_()
				consume_21_()
				local res = parseBase("hexadecimal", hexDigit_3f_1, 16)
				if negative then
					res = (0 - res)
				end
				val = res
			elseif ((char == "#") and (lower1(charAt1(str, succ1(offset))) == "b")) then
				consume_21_()
				consume_21_()
				local res = parseBase("binary", binDigit_3f_1, 2)
				if negative then
					res = (0 - res)
				end
				val = res
			elseif ((char == "#") and terminator_3f_1(lower1(charAt1(str, succ1(offset))))) then
				val = doNodeError_21_1(logger, "Expected hexadecimal (#x) or binary (#b) digit.", range(position()), "The '#' character is used for various number representations, such as binary\nand hexadecimal digits.\n\nIf you're looking for the '#' function, this has been replaced with 'n'. We\napologise for the inconvenience.", range(position()), "# must be followed by x or b")
			elseif (char == "#") then
				consume_21_()
				val = doNodeError_21_1(logger, "Expected hexadecimal (#x) or binary (#b) digit specifier.", range(position()), "The '#' character is used for various number representations, namely binary\nand hexadecimal digits.", range(position()), "# must be followed by x or b")
			else
				local temp1 = nil
				while between_3f_1(charAt1(str, succ1(offset)), "0", "9") do
					consume_21_()
				end
				if (charAt1(str, succ1(offset)) == ".") then
					consume_21_()
					local temp1 = nil
					while between_3f_1(charAt1(str, succ1(offset)), "0", "9") do
						consume_21_()
					end
				end
				char = charAt1(str, succ1(offset))
				if ((char == "e") or (char == "E")) then
					consume_21_()
					char = charAt1(str, succ1(offset))
					if ((char == "-") or (char == "+")) then
						consume_21_()
					end
					local temp1 = nil
					while between_3f_1(charAt1(str, succ1(offset)), "0", "9") do
						consume_21_()
					end
				end
				val = tonumber1(sub1(str, start["offset"], offset))
			end
			appendWith_21_(({["tag"]="number",["value"]=val}), start)
			char = charAt1(str, succ1(offset))
			if terminator_3f_1(char) then
			else
				consume_21_()
				doNodeError_21_1(logger, format1("Expected digit, got %s", (function()
					if (char == "") then
						return "eof"
					else
						return char
					end
				end)()
				), range(position()), nil, range(position()), "Illegal character here. Are you missing whitespace?")
			end
		elseif (char == "\"") then
			local start = position()
			local startCol = succ1(column)
			local buffer = ({tag = "list", n = 0})
			consume_21_()
			char = charAt1(str, offset)
			local temp1 = nil
			while (char ~= "\"") do
				if (column == 1) then
					local running = true
					local lineOff = offset
					local temp2 = nil
					while (running and (column < startCol)) do
						if (char == " ") then
							consume_21_()
						elseif (char == "\n") then
							consume_21_()
							pushCdr_21_1(buffer, "\n")
							lineOff = offset
						elseif (char == "") then
							running = false
						else
							putNodeWarning_21_1(logger, format1("Expected leading indent, got %q", char), range(position()), "You should try to align multi-line strings at the initial quote\nmark. This helps keep programs neat and tidy.", range(start), "String started with indent here", range(position()), "Mis-aligned character here")
							pushCdr_21_1(buffer, sub1(str, lineOff, pred1(offset)))
							running = false
						end
						char = charAt1(str, offset)
					end
				end
				if (char == "") then
					local start1 = range(start)
					local finish = range(position())
					eofError_21_1(cont, logger, "Expected '\"', got eof", finish, nil, start1, "string started here", finish, "end of file here")
				elseif (char == "\\") then
					consume_21_()
					char = charAt1(str, offset)
					if (char == "\n") then
					elseif (char == "a") then
						pushCdr_21_1(buffer, "\7")
					elseif (char == "b") then
						pushCdr_21_1(buffer, "\8")
					elseif (char == "f") then
						pushCdr_21_1(buffer, "\12")
					elseif (char == "n") then
						pushCdr_21_1(buffer, "\n")
					elseif (char == "r") then
						pushCdr_21_1(buffer, "\13")
					elseif (char == "t") then
						pushCdr_21_1(buffer, "\9")
					elseif (char == "v") then
						pushCdr_21_1(buffer, "\11")
					elseif (char == "\"") then
						pushCdr_21_1(buffer, "\"")
					elseif (char == "\\") then
						pushCdr_21_1(buffer, "\\")
					elseif ((char == "x") or ((char == "X") or between_3f_1(char, "0", "9"))) then
						local start1 = position()
						local val
						if ((char == "x") or (char == "X")) then
							consume_21_()
							local start2 = offset
							if hexDigit_3f_1(charAt1(str, offset)) then
							else
								digitError_21_1(logger, range(position()), "hexadecimal", charAt1(str, offset))
							end
							if hexDigit_3f_1(charAt1(str, succ1(offset))) then
								consume_21_()
							end
							val = tonumber1(sub1(str, start2, offset), 16)
						else
							local start2 = position()
							local ctr = 0
							char = charAt1(str, succ1(offset))
							local temp2 = nil
							while ((ctr < 2) and between_3f_1(char, "0", "9")) do
								consume_21_()
								char = charAt1(str, succ1(offset))
								ctr = (ctr + 1)
							end
							val = tonumber1(sub1(str, start2["offset"], offset))
						end
						if (val >= 256) then
							doNodeError_21_1(logger, "Invalid escape code", range(start1), nil, range(start1, position()), _2e2e_2("Must be between 0 and 255, is ", val))
						end
						pushCdr_21_1(buffer, char1(val))
					elseif (char == "") then
						eofError_21_1(cont, logger, "Expected escape code, got eof", range(position()), nil, range(position()), "end of file here")
					else
						doNodeError_21_1(logger, "Illegal escape character", range(position()), nil, range(position()), "Unknown escape character")
					end
				else
					pushCdr_21_1(buffer, char)
				end
				consume_21_()
				char = charAt1(str, offset)
			end
			appendWith_21_(({["tag"]="string",["value"]=concat1(buffer)}), start)
		elseif (char == ";") then
			local temp1 = nil
			while ((offset <= length) and (charAt1(str, succ1(offset)) ~= "\n")) do
				consume_21_()
			end
		else
			local start = position()
			local key = (char == ":")
			char = charAt1(str, succ1(offset))
			local temp1 = nil
			while _21_1(terminator_3f_1(char)) do
				consume_21_()
				char = charAt1(str, succ1(offset))
			end
			if key then
				appendWith_21_(({["tag"]="key",["value"]=sub1(str, succ1(start["offset"]), offset)}), start)
			else
				append_21_("symbol", start)
			end
		end
		consume_21_()
	end
	append_21_("eof")
	return out
end)
parse1 = (function(logger, toks, cont)
	local head = ({tag = "list", n = 0})
	local stack = ({tag = "list", n = 0})
	local append_21_ = (function(node)
		return pushCdr_21_1(head, node)
	end)
	local push_21_ = (function()
		local next = ({tag = "list", n = 0})
		pushCdr_21_1(stack, head)
		append_21_(next)
		head = next
		return nil
	end)
	local pop_21_ = (function()
		head["open"] = nil
		head["close"] = nil
		head["auto-close"] = nil
		head["last-node"] = nil
		head = last1(stack)
		return popLast_21_1(stack)
	end)
	local temp = n1(toks)
	local temp1 = nil
	local temp2 = 1
	while (temp2 <= temp) do
		local tok = toks[temp2]
		local tag = tok["tag"]
		local autoClose = false
		local previous = head["last-node"]
		local tokPos = tok["range"]
		local temp3
		if (tag ~= "eof") then
			if (tag ~= "close") then
				if head["range"] then
					temp3 = (tokPos["start"]["line"] ~= head["range"]["start"]["line"])
				else
					temp3 = true
				end
			else
				temp3 = false
			end
		else
			temp3 = false
		end
		if temp3 then
			if previous then
				local prevPos = previous["range"]
				if (tokPos["start"]["line"] ~= prevPos["start"]["line"]) then
					head["last-node"] = tok
					if (tokPos["start"]["column"] ~= prevPos["start"]["column"]) then
						putNodeWarning_21_1(logger, "Different indent compared with previous expressions.", tok, "You should try to maintain consistent indentation across a program,\ntry to ensure all expressions are lined up.\nIf this looks OK to you, check you're not missing a closing ')'.", prevPos, "", tokPos, "")
					end
				end
			else
				head["last-node"] = tok
			end
		end
		if ((tag == "string") or ((tag == "number") or ((tag == "symbol") or (tag == "key")))) then
			append_21_(tok)
		elseif (tag == "open") then
			push_21_()
			head["open"] = tok["contents"]
			head["close"] = tok["close"]
			head["range"] = ({["start"]=tok["range"]["start"],["name"]=tok["range"]["name"],["lines"]=tok["range"]["lines"]})
		elseif (tag == "open-struct") then
			push_21_()
			head["open"] = tok["contents"]
			head["close"] = tok["close"]
			head["range"] = ({["start"]=tok["range"]["start"],["name"]=tok["range"]["name"],["lines"]=tok["range"]["lines"]})
			append_21_(({["tag"]="symbol",["contents"]="struct-literal",["range"]=head["range"]}))
		elseif (tag == "close") then
			if empty_3f_1(stack) then
				doNodeError_21_1(logger, format1("'%s' without matching '%s'", tok["contents"], tok["open"]), tok, nil, getSource1(tok), "")
			elseif head["auto-close"] then
				doNodeError_21_1(logger, format1("'%s' without matching '%s' inside quote", tok["contents"], tok["open"]), tok, nil, head["range"], "quote opened here", tok["range"], "attempting to close here")
			elseif (head["close"] ~= tok["contents"]) then
				doNodeError_21_1(logger, format1("Expected '%s', got '%s'", head["close"], tok["contents"]), tok, nil, head["range"], format1("block opened with '%s'", head["open"]), tok["range"], format1("'%s' used here", tok["contents"]))
			else
				head["range"]["finish"] = tok["range"]["finish"]
				pop_21_()
			end
		elseif ((tag == "quote") or ((tag == "unquote") or ((tag == "syntax-quote") or ((tag == "unquote-splice") or (tag == "quasiquote"))))) then
			push_21_()
			head["range"] = ({["start"]=tok["range"]["start"],["name"]=tok["range"]["name"],["lines"]=tok["range"]["lines"]})
			append_21_(({["tag"]="symbol",["contents"]=tag,["range"]=tok["range"]}))
			autoClose = true
			head["auto-close"] = true
		elseif (tag == "eof") then
			if (0 ~= n1(stack)) then
				eofError_21_1(cont, logger, format1("Expected '%s', got 'eof'", head["close"]), tok, nil, head["range"], "block opened here", tok["range"], "end of file here")
			end
		else
			error1(_2e2e_2("Unsupported type", tag))
		end
		if autoClose then
		else
			local temp3 = nil
			while head["auto-close"] do
				if empty_3f_1(stack) then
					doNodeError_21_1(logger, format1("'%s' without matching '%s'", tok["contents"], tok["open"]), tok, nil, getSource1(tok), "")
				end
				head["range"]["finish"] = tok["range"]["finish"]
				pop_21_()
			end
		end
		temp2 = (temp2 + 1)
	end
	return head
end)
read1 = (function(x, path)
	return parse1(void1, lex1(void1, x, (path or "")))
end)
index1 = ({tag = "list", n = 0})
desc1 = ({})
unquoteList1 = (function(xs)
	local temp = type1(xs)
	if (temp == "key") then
		return sub1(pretty1(xs), 2)
	elseif (temp == "string") then
		return sub1(pretty1(xs), 2, -2)
	elseif (temp == "list") then
		return map1(unquoteList1, xs)
	elseif eq_3f_1(temp, true) then
		return printError_21_1("Invalid value in list.")
	else
		return error1(_2e2e_2("Pattern matching failure!\nTried to match the following patterns against ", pretty1(temp), ", but none matched.\n", "  Tried: `\"key\"`\n  Tried: `\"string\"`\n  Tried: `\"list\"`\n  Tried: `true`"))
	end
end)
quotedlist_2d3e_struct1 = (function(xs)
	local xxs = unquoteList1(xs)
	local sct = ({})
	local idx = 1
	local temp = nil
	while (idx <= n1(xxs)) do
		sct[nth1(xxs, idx)] = nth1(xxs, (idx + 1))
		idx = (idx + 1)
		idx = (idx + 1)
	end
	return sct
end)
parseEntry1 = (function(xs)
	if eq_3f_1(car2(xs), ({ tag="symbol", contents="pkg"})) then
		return quotedlist_2d3e_struct1(cdr2(xs))
	else
		return printError_21_1(_2e2e_2("Invalid entry ", pretty1(xs), "."))
	end
end)
setupEnv_21_1 = (function(override)
	if _21_1(pathExists_3f_1("jugs.lisp")) then
		printError_21_1(_2e2e_2("Could not find ", "jugs.lisp", ". Are you in the correct directory?"))
	end
	map1((function(path)
		if _21_1(dirExists_3f_1(path)) then
			return makeDir_21_1(path)
		else
			return nil
		end
	end), list1(".jugs", cache1, env1))
	loadDesc_21_1()
	local remoteIndexStr = readPath_21_1((function(temp)
		if temp then
			return temp
		else
			if pathExists_3f_1(".jugsindex") then
				return ".jugsindex"
			else
				return "https://gitlab.com/urn/jugs/raw/master/jugs-packages.lisp"
			end
		end
	end)((function()
		if (override == true) then
			return "https://gitlab.com/urn/jugs/raw/master/jugs-packages.lisp"
		else
			return override
		end
	end)()
	))
	local indexList = loadIndex_21_1((remoteIndexStr or printError_21_1("Could not retrieve index.")))
	local indexStr = concat1(map1((function(xs)
		return pretty1(xs)
	end), indexList), "\n")
	if (_21_1(pathExists_3f_1(".jugsindex")) or (indexStr ~= readPath_21_1(indexPrev1))) then
		print1("Updating index...")
		rmDir_21_1(env1)
		makeDir_21_1(env1)
		writeFile_21_1(indexPrev1, indexStr)
	end
	writeFile_21_1(".jugsindex", indexStr)
	local temp = n1(indexList)
	local temp1 = nil
	local temp2 = 1
	while (temp2 <= temp) do
		pushCdr_21_1(index1, parseEntry1((indexList[temp2])))
		temp2 = (temp2 + 1)
	end
	return installPkgs_21_1(cons2("urn", (desc1["deps"] or ({tag = "list", n = 0}))))
end)
loadIndex_21_1 = (function(indexStr)
	local indexList = read1(indexStr)
	local xs = ({tag = "list", n = 0})
	local temp = n1(indexList)
	local temp1 = nil
	local temp2 = 1
	while (temp2 <= temp) do
		local indexEntry = indexList[temp2]
		local temp3
		if eq_3f_1(car2(indexEntry), ({ tag="symbol", contents="remote"})) then
			temp3 = loadIndex_21_1(cadr1(indexEntry))
		else
			temp3 = list1(indexEntry)
		end
		local temp4 = n1(temp3)
		local temp5 = nil
		local temp6 = 1
		while (temp6 <= temp4) do
			pushCdr_21_1(xs, (temp3[temp6]))
			temp6 = (temp6 + 1)
		end
		temp2 = (temp2 + 1)
	end
	return xs
end)
loadDesc_21_1 = (function()
	local descEntries = read1(readPath_21_1("jugs.lisp"))
	if (n1(descEntries) ~= 1) then
		printError_21_1(_2e2e_2("More than 1 entry in ", "jugs.lisp", "."))
	end
	local parsedDesc = parseEntry1(car2(descEntries))
	if parsedDesc["entry"] then
		parsedDesc["entry"] = quotedlist_2d3e_struct1(parsedDesc["entry"])
	end
	local temp = keys1(parsedDesc)
	local temp1 = n1(temp)
	local temp2 = nil
	local temp3 = 1
	while (temp3 <= temp1) do
		local key = temp[temp3]
		desc1[key] = parsedDesc[key]
		temp3 = (temp3 + 1)
	end
	return nil
end)
installPkgs_21_1 = (function(pkgs)
	local temp = n1(pkgs)
	local temp1 = nil
	local temp2 = 1
	while (temp2 <= temp) do
		local pkg = pkgs[temp2]
		local pkgDesc = car2(filter1((function(xs)
			if eq_3f_1(xs["name"], pkg) then
				return xs
			else
				return nil
			end
		end), index1))
		if _21_1(pkgDesc) then
			printError_21_1(_2e2e_2("Could not find package \"", pkg, "\"."))
		end
		if _21_1(dirExists_3f_1(_2e2e_2(env1, "/", pkg))) then
			print1(_2e2e_2("Fetching ", pkg, "..."))
			cloneGit_21_1(car2(pkgDesc["git"]), cadr1(pkgDesc["git"]), (caddr1(pkgDesc["git"]) or ""), _2e2e_2(env1, "/", pkg), cache1)
		end
		if pkgDesc["deps"] then
			installPkgs_21_1(pkgDesc["deps"])
		end
		temp2 = (temp2 + 1)
	end
	return nil
end)
argsFlags1 = args1["flags"]
urnFlags1 = _2e2e_2(concat1((function()
	if argsFlags1 then
		return car2(split1(argsFlags1, "--"))
	else
		return ({tag = "list", n = 0})
	end
end)()
, " "), " -i ", absPath1(env1), " -i ", absPath1(env1), "/urn")
local temp
local temp1 = args1["runtime-args"]
if temp1 then
	temp = temp1
else
	if argsFlags1 then
		temp = cadr1(split1(argsFlags1, "--"))
	else
		temp = ({tag = "list", n = 0})
	end
end
runtimeArgs1 = (temp or ({tag = "list", n = 0}))
placePkg_21_1 = (function()
	local pkgDir = _2e2e_2(env1, "/", desc1["name"])
	rmDir_21_1(pkgDir)
	makeDir_21_1(pkgDir)
	return copyDir_21_1("*", pkgDir)
end)
initPkg_21_1 = (function(name)
	if _21_1(name) then
		printExit_21_1("jugs init <name>")
	end
	makeDir_21_1(name)
	writeFile_21_1(_2e2e_2(name, "/", "jugs.lisp"), _2e2e_2("(pkg\n  :name \"", name, "\"\n  :entry ( :main \"main.lisp\" ) )"))
	return writeFile_21_1(_2e2e_2(name, "/main.lisp"), "(print! \"Hello, world!\")")
end)
buildPkg_21_1 = (function(deriviation, run)
	local deriv = (deriviation or "main")
	local pkgName = desc1["name"]
	local pkgEntry = (desc1["entry"] or ({}))
	local pkgEntryFile = pkgEntry[deriv]
	local descCompilerFlags = concat1((pkgEntry["compiler-flags"] or ({tag = "list", n = 0})), " ")
	local binPath = _2e2e_2(".jugs", "/", desc1["name"])
	if (pkgEntry == nil) then
		printError_21_1(_2e2e_2("No entry point found for \"", deriv, "\"."))
	end
	placePkg_21_1()
	rmFile_21_1(_2e2e_2(binPath, ".lua"))
	print1(_2e2e_2("Building ", desc1["name"], "..."))
	execCmd_21_1(_2e2e_2(absPath1(compiler1), " ", absPath1(pkgEntryFile), " ", urnFlags1, " ", descCompilerFlags, " --out ", absPath1(binPath), " --chmod --shebang"), true)
	if _21_1(pathExists_3f_1(_2e2e_2(binPath, ".lua"))) then
		return printError_21_1("Compilation failed.")
	else
		return nil
	end
end)
runPkg_21_1 = (function()
	return exit1(execCmdCode_21_1(_2e2e_2(absPath1(".jugs"), "/", desc1["name"], ".lua ", concat1(runtimeArgs1, " ")), true))
end)
runRepl_21_1 = (function()
	placePkg_21_1()
	return exit1(execCmdCode_21_1(_2e2e_2(absPath1(compiler1), " ", urnFlags1), true))
end)
run_21_1 = (function()
	local method = args1["method"]
	local temp = car2(method)
	if (temp == "init") then
		return initPkg_21_1(cadr1(method))
	elseif (temp == "build") then
		setupEnv_21_1()
		return buildPkg_21_1(cadr1(method))
	elseif (temp == "run") then
		setupEnv_21_1()
		buildPkg_21_1(cadr1(method))
		return runPkg_21_1()
	elseif (temp == "repl") then
		setupEnv_21_1()
		return runRepl_21_1()
	elseif (temp == "update-index") then
		return setupEnv_21_1((cadr1(method) or true))
	else
		return printExit_21_1(_2e2e_2((function()
			if car2(method) then
				return "Unknown method."
			else
				return "jugs init <name> \njugs build [entry]\njugs run [entry]\njugs repl\njugs update-index [path]"
			end
		end)()
		, "\nCheck out --help for more info."))
	end
end)
return run_21_1()
