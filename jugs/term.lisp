(import extra/term (colored))
(import lua/os os)

(defun print-error! (msg)
  (print! (.. (colored 31 "[ERROR] ") msg))
  (os/exit 1))

(defun print-exit! (msg)
  (print! msg)
  (os/exit 1))

(defun print-warning! (msg)
  (print! (.. (colored 33 "[WARNING] ") msg)))
