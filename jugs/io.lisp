(import extra/io (read-all! write-all!))
(import lua/io (popen))
(import lua/os os)
(import jugs/cli cli)

(define pkg-dir (.> cli/args :dir))
(define silent true)


(defun exec-cmd-code! (command verbose)
  (caddr (list (os/execute (.. command (if (and silent (! verbose)) " >/dev/null 2>&1" ""))))))

(defun exec-cmd! (command verbose)
  (= (exec-cmd-code! command verbose) 0))

(defun abs-path (path)
  (if (= (string/char-at path 1) "/")
    path
    (.. pkg-dir "/" path)))

(defun read-url! (url) :hidden
  (with (program (if (exec-cmd! "which curl")
                   "curl -s "
                   "wget -qO- "))
    (when-with (handle (popen (.. program url)))
      (with (result (self handle :read "*a"))
        (self handle :close)
        (when (/= (n result) 0)
          result)))))

(defun read-path! (path)
  (if (or (= (string/sub path 1 7) "http://") (= (string/sub path 1 8) "https://"))
    (read-url! path)
    (read-all! (abs-path path))))

(defun write-file! (file contents)
  (write-all! (abs-path file) contents))

(defun path-exists? (path)
  (/= (read-path! path) nil))

(defun dir-exists? (dir)
  (exec-cmd! (.. "test -d " (abs-path dir))))

(defun make-dir! (dir)
  (os/execute (.. "mkdir -p " (abs-path dir))))

(defun copy-dir! (from to)
  (exec-cmd! (.. "cp -r " (abs-path from) " " (abs-path to))))

(defun rm-dir! (dir)
  (exec-cmd! (.. "rm -rf " (abs-path dir))))

(defun rm-file! (file)
  (exec-cmd! (.. "rm " (abs-path file))))

(defun clone-git! (url rev path dest-path cache-path)
  (let* [(local-repo-path (.. cache-path "/" (string/gsub url "%W" "-")))
         (repo-path (abs-path local-repo-path))]
    (if (dir-exists? local-repo-path)
      (progn
        (exec-cmd! (.. "cd " repo-path "; git checkout master"))
        (exec-cmd! (.. "cd " repo-path "; git pull")))
      (exec-cmd! (.. "git clone " url " " repo-path)))
    (exec-cmd! (.. "cd " repo-path "; git checkout " rev))
    (copy-dir! (.. local-repo-path "/" path) dest-path)))
