(import lua/os ())
(import lua/io (popen))

(define default-index "https://gitlab.com/urn/jugs/raw/master/jugs-packages.lisp")
(define jugs ".jugs")
(define cache
  (.. (with (uname (or (when-let* [(h (popen "uname"))]
                         (self h :read "*l"))
                       nil))
        (cond
          [(= uname "Darwin")
            (.. (getenv "HOME") "/Library/Application Support")]
          [(= uname "Linux")
            (or (getenv "XDG_DATA_DIR")
                (.. (getenv "HOME") "/.local/share"))]))
      "/jugs/cache"))
(define env (.. jugs "/env"))
(define index ".jugsindex")
(define index-prev (.. jugs "/prev-index"))
(define desc "jugs.lisp")
(define compiler (.. env "/urn/bin/urn.lua"))
