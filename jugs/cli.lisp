(import extra/argparse argparse)

(define methods-help "jugs init <name> 
                      jugs build [entry]
                      jugs run [entry]
                      jugs repl
                      jugs update-index [path]")

(define args (with (spec (argparse/create))
               (argparse/add-help! spec)
               (argparse/add-argument! spec '("method")
                 :help (.. "The method:\n" methods-help))
               (argparse/add-argument! spec '("--dir" "-d")
                 :help "The program directory."
                 :default "."
                 :narg 1)
               ;; readd this when i make a list/split
               (argparse/add-argument! spec '("--flags")
                 :help "Flags to pass to the urn compiler."
                 :narg "*"
                 :all true)
               (argparse/add-argument! spec '("--runtime-args" "--")
                 :help "Arguments to pass to the program."
                 :narg "*"
                 :all true)
               (argparse/parse! spec)))
