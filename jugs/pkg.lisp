(import jugs/io ())
(import jugs/cli cli)
(import jugs/term ())
(import jugs/path path)
(import urn/parser)

(define index '())
(define desc {})


(defun unquote-list (xs)
  (case (type xs)
    ["key" (string/sub (pretty xs) 2)]
    ["string" (string/sub (pretty xs) 2 -2)]
    ["list" (map unquote-list xs)]
    [true (print-error! "Invalid value in list.")]))

(defun quotedlist->struct (xs)
  (let* [(xxs (unquote-list xs))
         (sct {})
         (idx 1)]
    (while (<= idx (n xxs))
      (.<! sct (nth xxs idx) (nth xxs (+ idx 1)))
      (inc! idx)
      (inc! idx))
    sct))

(defun parse-entry (xs)
  (if (eq? (car xs) 'pkg)
    (quotedlist->struct (cdr xs))
    (print-error! (.. "Invalid entry " (pretty xs) "."))))


(defun setup-env! (override)
  (when (! (path-exists? path/desc))
    (print-error! (.. "Could not find " path/desc ". Are you in the correct directory?")))
  (map
    (lambda (path)
      (when (! (dir-exists? path))
        (make-dir! path)))
    (list path/jugs path/cache path/env))
  (load-desc!)
  (let* [(remote-index-str (read-path! (or (if (= override true) path/default-index override)
                                           (if (path-exists? path/index)
                                             path/index
                                             path/default-index))))
         (index-list (load-index! (if remote-index-str
                                    remote-index-str
                                    (print-error! "Could not retrieve index."))))
         (index-str (concat (map (lambda (xs) (pretty xs)) index-list) "\n"))]
    (when (or (! (path-exists? path/index)) (/= index-str (read-path! path/index-prev))) ; invalidate
      (print! "Updating index...")
      (rm-dir! path/env)
      (make-dir! path/env)
      (write-file! path/index-prev index-str))
    (write-file! path/index index-str)
    (for-each index-entry index-list
      (push-cdr! index (parse-entry index-entry))))
  (install-pkgs! (cons "urn" (or (.> desc :deps) '()))))

(defun load-index! (index-str)
  (let* [(index-list (urn/parser/read index-str))
         (xs '())]
    (for-each index-entry index-list
      (for-each entry (if (eq? (car index-entry) 'remote)
                        (load-index! (cadr index-entry))
                        (list index-entry))
        (push-cdr! xs entry)))
    xs))

(defun load-desc! ()
  (with (desc-entries (urn/parser/read (read-path! path/desc)))  
    (when (/= (n desc-entries) 1)
      (print-error! (.. "More than 1 entry in " path/desc ".")))
    (with (parsed-desc (parse-entry (car desc-entries)))
      (when (.> parsed-desc :entry)
        (.<! parsed-desc :entry (quotedlist->struct (.> parsed-desc :entry))))
      (for-each key (keys parsed-desc)
        (.<! desc key (.> parsed-desc key))))))

(defun install-pkgs! (pkgs)
  (for-each pkg pkgs
    (with (pkg-desc (car (filter (lambda (xs) (when (eq? (.> xs :name) pkg) xs)) index)))
      (when (! pkg-desc)
        (print-error! (.. "Could not find package \"" pkg "\".")))
      (when (! (dir-exists? (.. path/env "/" pkg)))
        (print! (.. "Fetching " pkg "..."))
        (clone-git! (car (.> pkg-desc :git)) (cadr (.> pkg-desc :git)) (or (caddr (.> pkg-desc :git)) "") (.. path/env "/" pkg) path/cache))
      (when (.> pkg-desc :deps)
        (install-pkgs! (.> pkg-desc :deps))))))

; i know what i'm doing
