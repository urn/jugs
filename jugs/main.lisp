(import jugs/term (print-exit!))
(import jugs/pkg (setup-env!))
(import jugs/cli (args methods-help))
(import jugs/method (init-pkg! build-pkg! run-pkg! run-repl!))


(defun run! ()
  (with (method (.> args :method))
    (case (car method)
      ["init" (init-pkg! (cadr method))]
      ["build" (progn 
                 (setup-env!)
                 (build-pkg! (cadr method)))]
      ["run" (progn
               (setup-env!)
               (build-pkg! (cadr method))
               (run-pkg!))]
      ["repl" (progn
                (setup-env!)
                (run-repl!))]
      ["update-index" (setup-env! (or (cadr method) true))]
      [_ (print-exit! (.. (if (car method) "Unknown method."
                                           methods-help)
                          "\nCheck out --help for more info."))])))

(run!)
